import requests
import datetime
from time import strftime, gmtime

# r = requests.get('https://hacker-news.firebaseio.com/v0/maxitem.json')
# r.json()

# id: 16083711
# time: 1515210034


def timeof(id):
  item = requests.get('https://hacker-news.firebaseio.com/v0/item/' + str(id) + '.json').json()
  tz = item['time']
  return tz
  # fmt = strftime('%a, %d %b %Y %H:%M:%S', gmtime(tz))
  # return str(tz) + ' ' + fmt

def fmtgmt(tz):
  return strftime('%a, %d %b %Y %H:%M:%S', gmtime(tz))

# close to new year: 16045191
# item = requests.get('https://hacker-news.firebaseio.com/v0/item/16043280.json?print=pretty').json()

# tz = item['time']

# fmt = strftime('%a, %d %b %Y %H:%M:%S', gmtime(tz))

# print(tz, fmt)

tz = timeof(13293889)
print(tz, fmtgmt(tz))


# notes:
# using GMT
#
# last item of 2017:
# id: 16043766 
# tz: 1514764767 (Sun, 31 Dec 2017 23:59:27)
#
# first item of 2017:
# id: 13293889
# tz: 1483228806 (Sun, 01 Jan 2017 00:00:06)
#
# ID diff: 2749877
# about 3 million HTTP requests for item data
