// pool of http clients (fetchers)
// channel to send a comment
// database buffer, save 100 items, and then batch save
//
package main

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"net/http"
	"os"
	"regexp"
	"strings"
	"sync"
	"time"

	"database/sql"
	_ "github.com/lib/pq"
)

const (
	BatchSize        = 500
	TableName        = "comment_whtml"
	CreateTableQuery = `CREATE TABLE IF NOT EXISTS ` + TableName + ` (
		id int,
		timestamp timestamp,
		text text,
		username varchar(255)
	)`
	NumColumns = 4
)

type Comment struct {
	Type     string `json:"type"`
	Id       int    `json:"id"`
	Time     int    `json:"time"`
	Text     string `json:"text"`
	Username string `json:"by"`
	Deleted  bool   `json:"deleted"`
}

type Fetcher struct {
	client *http.Client
	recv   chan Comment
	retry  chan int
	done   chan Fetcher

	// Flag for retries
	isUsed bool
}

func (f Fetcher) fetch(id int) {
	uri := fmt.Sprintf("https://hacker-news.firebaseio.com/v0/item/%d.json", id)

	resp, err := f.client.Get(uri)
	if err != nil {
		fmt.Printf("ID %d: %s\n", id, err)
		f.retry <- id
		f.done <- f
		return
	}
	defer resp.Body.Close()

	var c Comment
	err = json.NewDecoder(resp.Body).Decode(&c)
	if err != nil {
		fmt.Printf("ID %d: %s\n", id, err)
		f.retry <- id
		f.done <- f
		return
	}

	f.recv <- c
	f.done <- f
}

func genQuery(table string, rows int) string {
	r := make([]string, rows)

	for i := 0; i < rows; i++ {
		j := i*NumColumns + 1
		r[i] = fmt.Sprintf("($%d,to_timestamp($%d),$%d,$%d)", j, j+1, j+2, j+3)
	}

	h := fmt.Sprintf("INSERT INTO %s (id,timestamp,text,username) VALUES ", table)
	return h + strings.Join(r, ",")
}

func fetch() {
	// first is 13293889
	// last is 16043766
	begin := 13293889 // 13309273 // 13293889
	end := 16043767
	pool := 32
	count := end - begin // 2749877
	poolend := begin + pool

	recv := make(chan Comment)
	errs := make(chan int)
	done := make(chan Fetcher)

	retry := make([]int, 0)

	// Initialize workers
	for id := begin; id < poolend; id++ {
		client := &http.Client{}

		f := Fetcher{
			client: client,
			recv:   recv,
			retry:  errs,
			done:   done,
		}
		go f.fetch(id)
	}

	go func() {
		ct := 1
		for {
			select {
			case id := <-errs:
				retry = append(retry, id)
			case c := <-recv:
				if !c.Deleted && c.Type == "comment" {
					queueComment(c, false)
				}
			}
			ct++
		}
	}()

	var f Fetcher
	var retryId int
	fetchers := make([]Fetcher, 0)

	// Fetch rest
	for i := 0; i < count; i++ {
		f = <-done

		if i < count-pool {
			go f.fetch(poolend + i)
		} else {
			fetchers = append(fetchers, f)
		}
	}

	for i := 0; i < 64; i++ {
		retry = append(retry, 13293905)
	}

	// Re-send fetchers
	go func() {
		var f Fetcher
		for len(fetchers) > 0 {
			f, fetchers = fetchers[0], fetchers[1:]
			done <- f
		}
	}()

	// Retry errors
	sent := 0
	for {
		f = <-done
		if f.isUsed {
			sent--
		}
		f.isUsed = false

		if len(retry) > 0 {
			retryId, retry = retry[0], retry[1:] // pop
			fmt.Printf("retrying %d...\n", retryId)
			sent++

			f.isUsed = true
			go f.fetch(retryId)
		}

		if sent == 0 {
			break
		}
	}
}

// DB reference and end-flag.
var db *sql.DB
var dbdone = make(chan error)

func main() {
	log.Println("Starting...")
	start := time.Now()

	var err error
	connStr := os.Getenv("DATABASE_URI")
	db, err = sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(CreateTableQuery)
	if err != nil {
		log.Fatal(err)
	}

	fetch()

	go queueComment(Comment{}, true)
	<-dbdone

	end := time.Now()
	log.Println("Finishing...")
	log.Printf("Elapsed time: %s\n", end.Sub(start))
}

// DB batching and lock variables.
var batch = make([]interface{}, BatchSize*NumColumns)
var rowIndex = 0
var dbLock = sync.Mutex{}
var batchCount = 0

var nullChar = regexp.MustCompile("\\x00")
// var tags = regexp.MustCompile("<[a-z/]+>")

func queueComment(c Comment, isStop bool) {
	dbLock.Lock()
	defer dbLock.Unlock()

	if !isStop {
		idx := rowIndex * NumColumns
		rowIndex++

		batch[idx] = c.Id
		batch[idx+1] = c.Time
		batch[idx+3] = c.Username

		text := html.UnescapeString(c.Text)
		b := nullChar.ReplaceAll([]byte(text), []byte(" "))
		// b = tags.ReplaceAll(b, []byte(" "))
		batch[idx+2] = string(b)
	}

	if isStop || rowIndex >= BatchSize {
		sl := batch[:rowIndex*NumColumns]
		save(sl, rowIndex)

		batchCount++
		fmt.Printf("Saved Batch %d to DB\n", batchCount)
		rowIndex = 0

		if isStop {
			dbdone <- nil
		}
	}
}

func save(values []interface{}, rows int) {
	insertQuery := genQuery(TableName, rowIndex)

	_, err := db.Exec(insertQuery, values...)
	if err != nil {
		log.Println(err)
	}
}
