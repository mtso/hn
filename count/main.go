// Read a page from comment db
//     log page skip and takes
// process comments into phrases (string slice chunks)
// filter phrases for meaningful ones
// queue increments up
// bulk upsert increments
package main

import (
	"fmt"
	"html"
	"regexp"
	"sort"
	"strings"

	"github.com/jdkato/prose/tag"
	"github.com/jdkato/prose/tokenize"
)

// func genInsertQuery(table string, rows int) string {
// 	// INSERT INTO table (token, count) VALUES ($1,$2),($3,$4)
// }

type Count struct{
	token string
	count int
}

type ByCount []Count

func (a ByCount) Len() int { return len(a) }
func (a ByCount) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a ByCount) Less(i, j int) bool { return a[i].count >= a[j].count }

// === Increment in DB ===
// queue
func increment(token string) {

}

func save() {

}

// === Processing ===
var wordttz = tokenize.NewWordBoundaryTokenizer()
var tags = regexp.MustCompile("(<[/a-z]+>)")
const maxWords = 5 // max words in a phrase
// process splits the comment text into phrases of 1 to 5 words
func process(text string) {
	text = html.UnescapeString(text)
	text = string(tags.ReplaceAll([]byte(text), []byte(" ")))

    words := wordttz.Tokenize(text)
    counts := make(map[string]int)

    for i := 0; i < len(words); i++ {
    	for j := 1; j <= maxWords && j+i < len(words); j++ {
    		phrase := words[i:i+j]
    		if !isMeaningful(phrase) {
    			continue
    		}

    		tt := strings.ToLower(strings.Join(phrase, " "))
    		increment(tt)
    	}
    }
}

// === Filter Func ===
var tagger = tag.NewPerceptronTagger()
var blacklist = map[string]bool {
	"DT": true,
}
// isMeaningful returns whether slice of strings are
// all unwanted words or not.
func isMeaningful(phrase []string) bool {
	isInBlacklist := 0
	for _, tok := range tagger.Tag(phrase) {
		if blacklist[tok.Tag] {
			isInBlacklist++
		}
	}
	return isInBlacklist != len(phrase)
}

func main() {
	text := html.UnescapeString("<i>To alleviate concerns associated with commercialization, compensation could be limited to fixed payments or to benefit packages that include insurance, tax deductions for travel, lodging and lost wages, or paid donor leave.</i><p>Is there a coherent argument for creating a &quot;market&quot; where sellers are banned from realizing the full market value of the thing they are selling? I guess people will say gouging, but that doesn&#x27;t really work for an organ market (2 kidneys are actually better than 1, there&#x27;s nothing short term or unusual about the shortage).")

	// fmt.Println(text)
	// fmt.Println()
	replaced := string(tags.ReplaceAll([]byte(text), []byte(" ")))

	// fmt.Println(replaced)
    words := wordttz.Tokenize(replaced)

    counts := make(map[string]int)

    for i := 0; i < len(words)-4; i++ {
    	// tt := strings.ToLower(words[i])
    	tt2 := strings.ToLower(strings.Join(words[i:i+1], " "))
    	tt3 := strings.ToLower(strings.Join(words[i:i+2], " "))
    	tt4 := strings.ToLower(strings.Join(words[i:i+3], " "))
    	tt5 := strings.ToLower(strings.Join(words[i:i+4], " "))

    	// counts[tt]++
    	counts[tt2]++
    	counts[tt3]++
    	counts[tt4]++
    	counts[tt5]++
    }

    l := len(words)
    tt4 := strings.ToLower(strings.Join(words[l-4:l], " "))
    tt3 := strings.ToLower(strings.Join(words[l-3:l], " "))
    tt2 := strings.ToLower(strings.Join(words[l-2:l], " "))
    tt1 := strings.ToLower(words[l-1])
    counts[tt4]++
    counts[tt3]++
    counts[tt2]++
    counts[tt1]++

    list := make([]Count, len(counts))

    i := 0
    for k, v := range counts {
    	list[i] = Count{k, v}
    	i++
    }

    sort.Sort(ByCount(list))
    for _, c := range list {
    	fmt.Printf("%d %s\n", c.count, c.token)
    }

    fmt.Printf("%d words, %d phrases", len(words), len(list))
}
