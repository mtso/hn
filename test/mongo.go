// Try batch upserting incrementing field.
//
// Single:
// db.getCollection('count').update({token: "foo"}, {$inc: {count: 1}}, {upsert: true})
//
package main

import (
	"fmt"
	"os"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Count struct {
	Token string
	Count int
}

func main() {
	connStr := os.Getenv("MONGODB_URI")
	session, err := mgo.Dial(connStr)
	if err != nil {
            panic(err)
    }
    defer session.Close()

    test := session.DB("test").C("count")

	bulk := test.Bulk()
	bulk.Upsert(bson.M{"token": "abc"}, bson.M{"$inc": bson.M{"count": 1}})
	bulk.Upsert(bson.M{"token": "bcd"}, bson.M{"$inc": bson.M{"count": 1}})
	r, err := bulk.Run()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("matched %d, modified %d\n", r.Matched, r.Modified)
	}

	// NOTE: Upserts from above persist
	//       so we cannot re-use the same Bulk object
	bulk.Upsert(bson.M{"token": "cde"}, bson.M{"$inc": bson.M{"count": 1}})
	bulk.Upsert(bson.M{"token": "def"}, bson.M{"$inc": bson.M{"count": 1}})
	r, err = bulk.Run()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("matched %d, modified %d\n", r.Matched, r.Modified)
	}
}
