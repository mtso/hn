package main

import (
	"fmt"
	"html"

	// "github.com/jdkato/prose/chunk"
	"github.com/jdkato/prose/tag"
	"github.com/jdkato/prose/tokenize"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var wordttz = tokenize.NewWordBoundaryTokenizer()
var tagger = tag.NewPerceptronTagger()
var blacklist = map[string]bool {
	"DT": true
}

type phrase []string

func (p phrase) isMeaningful() bool {
	isInBlacklist := 0
	for _, tok := range tagger.Tag(p) {
		if blacklist[tok.Tag] {
			isInBlacklist++
		}
	}
	return isInBlacklist != len(p)
}

func main() {
	text := html.UnescapeString("<i>To alleviate concerns associated with commercialization, compensation could be limited to fixed payments or to benefit packages that include insurance, tax deductions for travel, lodging and lost wages, or paid donor leave.</i><p>Is there a coherent argument for creating a &quot;market&quot; where sellers are banned from realizing the full market value of the thing they are selling? I guess people will say gouging, but that doesn&#x27;t really work for an organ market (2 kidneys are actually better than 1, there&#x27;s nothing short term or unusual about the shortage).")
	// tokenizer := tokenize.NewWordBoundaryTokenizer()


	// text := "An extraordinary artist. I'm glad he was able to be honored at the Chinese American Museum before his death."
	// text := "Commercial hives are normally sealed during winter in places that are in even the slightest way cold."
    // words := tokenize.NewTreebankWordTokenizer().Tokenize(text)

    // tokenizer := tokenize.NewWordBoundaryTokenizer()

    words := wordttz.Tokenize(text)

    for _, word := range words {
    	fmt.Println(word)
    }

    phr := phrase(words)
    if phr.isMeaningful() {
    	save<-phr
    }

    // tagger := tag.NewPerceptronTagger()
    // for _, tok := range tagger.Tag(words) {
    //     fmt.Println(tok.Text, tok.Tag)
    // }

    // words := tokenize.TextToWords("Commercial hives are normally sealed during winter in places that are in even the slightest way cold.")
    // regex := chunk.TreebankNamedEntities

    // tagger := tag.NewPerceptronTagger()
    // for _, entity := range chunk.Chunk(tagger.Tag(words), regex) {
    //     fmt.Println(entity) // [Go Google]
    // }
}
