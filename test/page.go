// read pages from comment DB
package main

import (
	"fmt"
	"os"
	"strings"

	"database/sql"
	_ "github.com/lib/pq"
)

const (
	FetchPageQuery = `SELECT id FROM comment_2 LIMIT $1 OFFSET $2`
)

type Comment struct {
	Id   int    `json:"id"`
	Time int    `json:"time"`
	Text string `json:"text"`
}

func print(comments []Comment) {
	ids := make([]string, len(comments))

	for i, c := range comments {
		idstring := fmt.Sprintf("%d", c.Id)
		ids[i] = idstring
	}

	fmt.Println("\n\n\n", strings.Join(ids, " "))
}

func fetch(done chan error, db *sql.DB, table string, page, count int) {
	rows, err := db.Query(FetchPageQuery, count, page * count)
	if err != nil {
		done<-err
		return
	}
	defer rows.Close()

	comments := make([]Comment, count)
	var c Comment
	i := 0
	for rows.Next() {
		if err := rows.Scan(&c.Id); err != nil {
			done<-err
			return
		}
		comments[i] = c
		i++
	}

	if err := rows.Err(); err != nil {
		done<-err
		return
	}

	print(comments)
	done<-nil
}

func main() {
	connStr := os.Getenv("DATABASE_URI")
	db, err := sql.Open("postgres", connStr)
	if err != nil { panic(err) }

	done := make(chan error)
	pageCount := 50
	pages := 5

	for i := 0; i < pages; i++ {
		go fetch(done, db, "comment_1", i, pageCount)
	}

	for i := 0; i < pages; i++ {
		e := <-done
		if e != nil {
			fmt.Println(e)
		}
	}
}
